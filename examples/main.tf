module "cloudflare-records_example-com" {
  source  = "git::https://gitlab.com/ganex-cloud/terraform/terraform-cloudflare-record.git?ref=master"
  zone_id = module.cloudflare-zone_example-com.this_id

  records = [
    {
      name    = "@"
      type    = "CNAME"
      value   = "XXXX"
      ttl     = 1
      proxied = true
    },
    {
      name    = "www"
      type    = "CNAME"
      value   = "XXXX"
      ttl     = 1
      proxied = true
    },
    {
      name                 = "@"
      type                 = "MX"
      priority             = 10
      value                = "inbound-smtp.us-east-1.amazonaws.com"
      ttl                  = 1
      proxied              = false
      resource_name_suffix = "1"
    },
    {
      name                 = "@"
      type                 = "MX"
      priority             = 10
      value                = "inbound-smtp.us-east-1.amazonaws.com"
      ttl                  = 1
      proxied              = false
      resource_name_suffix = "2"
    },
    {
      name                 = "@"
      type                 = "MX"
      priority             = 10
      value                = "inbound-smtp.us-east-1.amazonaws.com"
      ttl                  = 1
      proxied              = false
      resource_name_suffix = "3"
    },
    {
      name  = "*"
      type  = "CNAME"
      value = "XXXX"
    },
    {
      name  = "_amazonses"
      type  = "TXT"
      value = "3s+3l0W2Cgko01wnsdasdadNZMKGeKPTFdFHR0/fg+4yjBs8nY="
    }
  ]

  records_srv = [
    {
      name          = "test"
      ttl           = 1
      data_service  = "_sip"
      data_proto    = "_tls"
      data_name     = "test"
      data_priority = 0
      data_weight   = 0
      data_port     = 443
      data_target   = "example.com"
    }
  ]
}
