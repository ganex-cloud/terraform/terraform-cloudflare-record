variable "zone_id" {
  description = " (Required) The DNS zone ID to add the record to"
  type        = string
}

variable "records" {
  description = "A List of records to add to the zone (https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/record)"
  type        = list(any)
  default     = []
}

variable "records_srv" {
  description = "A List of SRV records to add to the zone"
  type        = list(any)
  default     = []
}
