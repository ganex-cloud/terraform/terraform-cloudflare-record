locals {
  recordsets     = { for rs in var.records : join(" ", compact(["${rs.name} ${rs.type}", lookup(rs, "resource_name_suffix", "")])) => rs }
  recordsets_srv = { for rs in var.records_srv : join(" ", compact(["${rs.name} srv", lookup(rs, "resource_name_suffix", "")])) => rs }
}

resource "cloudflare_record" "this" {
  for_each = var.zone_id != null && length(local.recordsets) > 0 ? local.recordsets : tomap({})
  zone_id  = var.zone_id
  name     = lookup(each.value, "name")
  value    = lookup(each.value, "value", null)
  type     = lookup(each.value, "type")
  ttl      = lookup(each.value, "ttl", 1)
  priority = lookup(each.value, "priority", null)
  proxied  = lookup(each.value, "proxied", false)
}

resource "cloudflare_record" "srv" {
  for_each = var.zone_id != null && length(local.recordsets_srv) > 0 ? local.recordsets_srv : tomap({})
  zone_id  = var.zone_id
  name     = lookup(each.value, "name")
  type     = "SRV"
  ttl      = lookup(each.value, "ttl", 1)
  priority = lookup(each.value, "priority", null)
  proxied  = false
  data = {
    name     = lookup(each.value, "data_name")
    port     = lookup(each.value, "data_port")
    priority = lookup(each.value, "data_priority")
    proto    = lookup(each.value, "data_proto")
    service  = lookup(each.value, "data_service")
    target   = lookup(each.value, "data_target")
    weight   = lookup(each.value, "data_weight")
  }
}
